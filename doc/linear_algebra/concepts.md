# CONCEPTOS BÁSICOS DE LAGEBRA LINEAL

## **A**

A [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **B**

[A](#a) B [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **C**

[A](#a) [B](#b) C [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### **Cuerpo o campo (field): K**

Sistema algebraico en que se pueden realizar *adiciones* y *multiplicaciones* cumpliendo ciertas propiedades.

Propiedades cumplidas de la multiplicación respecto a la adición:

- Asociativa
- Conmutativa
- Distributiva

Propiedades cumplidas respecto a la adición:

- Inverso aditivo
- Inverso multiplicativo
- Elemento neutro

Propiedades cumplidas respecto a la multiplicación:

- Elemento neutro

K es un conjunto con dos operaciones (adición y producto) que cumplen:

- **K es cerrado para la adición:** $`∀ a, b ∈ K → a + b ∈ K`$

- **K es cerrado para el producto:** $`∀ a, b ∈ K → a · b ∈ K`$

- **Asociatividad de la adición:** $`∀ a, b, c ∈ K → a + (b + c) = (a + b) + c`$

- **Asociatividad del producto:** $`∀ a, b, c ∈ K → a · (b · c ) = (a · b) · c`$

- **Conmutatividad de la adición:** $`∀ a, b ∈ K → a + b = b + a`$

- **Conmutatividad del producto:** $`∀ a, b ∈ K → a · b = b · a`$

- **Elemento neutro para la adición:** $`∀ a ∈ K ∃ b = 0 ∈ K : a + b = a`$

- **Elemento neutro para el producto:** $`∀ a ∈ K ∃ b = 1 ∈ K : a · b = a`$

- **Existe un elemento opuesto para la adición:** $`∀ a ∈ K ∃ b = -a ∈ K : a + b = 0`$

- **Existe un elemento inverso para el producto:** $`∀ a ≠ 0 ∈ K ∃ b = a^{-1} ∈ K : a · b = 1`$

- **Multiplicación distributiva respecto a la adición:** $`∀ a, b, c ∈ K → a · (b + c) = (a · b) + (a · c)`$

Se cumplen alguna reglas:

- $`(a · b)^{-1} =  a^{-1} · b^{-1}`$
- $`-a = (-1) · a`$
- $`-(a · b) = (-a) · b = a · (-b)`$
- $`a · 0 = 0`$

## **D**

[A](#a) [B](#b) [C](#c) D [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### Diagonal o diagonal principal

Sea $`A = M_{n}(K)`$ entonces su diagonal $`diag(A) = (a_{11}, a_{22}, ..., a_{nn})$`

## **E**

[A](#a) [B](#b) [C](#c) [D](#d) E [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### Ecalar

Número complejo, real o constante usado para para describir un fenómeno con magnitud pero sin la característica vectorial de dirección.

## **F**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) F [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **G**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) G [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **H**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) H [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **I**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) I [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **J**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) J [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **K**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) K [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **L**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) L [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **M**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) M [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### **Matriz: $`A ∈ M_{ixj} (K)`$**

Conjunto de escalares o elementos del cuerpo $`K^{1}`$ de m filas y n columnas. Consideraremos $`K = R`$ o $`K = C`$.

```math
A = (a_{ij}) = \begin{pmatrix} 
a_{11} & a_{12} & ... & a_{1n}\\
a_{21} & a_{22} & ... & a_{2n}\\
... & ... & ... &...\\
a_{m1} & a_{m2} & ... & a_{mn}
\end{pmatrix}
```

### **Matriz de orden n: $`M_{nxn} (K)`$ o $`M_{n} (K)`$**

Matriz cuadrada de n files y n columnas.


## **N**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) N [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **Ñ**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) Ñ [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **O**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) O [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **P**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) P [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **Q**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) Q [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **R**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) R [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **S**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) S [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### **Subdiagonal de una matriz de orden n**

Sea $`A = M_{n}(K)`$ entonces su subdiagonal es $`tr(A) = \sum_{i=1}^{n} a_{ii} = a_{11} + a_{22} + ... + a_{nn}$`

### **Superdiagonal de una matriz de orden n**

Sea $`A = M_{n}(K)`$ entonces su subdiagonal es $`tr(A) = \sum_{i=1}^{n} a_{ii} = a_{11} + a_{22} + ... + a_{nn}$`

## **T**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) T [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

### **Traza de una matriz de orden n**

Sea $`A = M_{n}(K)`$ entonces su traza es $`tr(A) = \sum_{i=1}^{n} a_{ii} = a_{11} + a_{22} + ... + a_{nn}$`

## **U**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) U [V](#v) [W](#w) [X](#x) [Y](#y) [Z](#z)

## **V**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) V [W](#w) [X](#x) [Y](#y) [Z](#z)

## **W**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) W [X](#x) [Y](#y) [Z](#z)

## **X**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) X [Y](#y) [Z](#z)

## **Y**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) Y [Z](#z)

## **Z**

[A](#a) [B](#b) [C](#c) [D](#d) [E](#e) [F](#f) [G](#g) [H](#h) [I](#i) [J](#j) [K](#k) [L](#l) [M](#m) [N](#n) [Ñ](#ñ) [O](#o) [P](#p) [Q](#q) [R](#r) [S](#s) [T](#t) [U](#u) [V](#v) [W](#w) [X](#x) [Y](#y) Z
