#!/usr/bin/env python
# -*- coding: utf-8 -*-


def is_antisymmetrical(m):
    """
    Check if a given matrix is antisymmetrical: transposed of the matrix equals negation of the matrix.
    
    Arguments:
        m {list} -- Matrix to be evaluated
    
    Raises:
        TypeError: If given argument is not a square matrix.
    
    Returns:
        Boolean -- True if m matrix is antisymmetrical.
    """
    if not is_square(m):
        raise TypeError
    return negative(m) == transpose(m)


def is_symmetrical(m):
    """
    Check if a matrix is symmetrical
    
    Arguments:
        m {list} -- Matrix to be evaluated.
    
    Raises:
        TypeError: If given argument is not a square matrix.
    
    Returns:
        Boolean -- True if is symmetrical. False otherwise.
    """
    if not is_square(m):
        raise TypeError
    return m == transpose(m)


def is_square(m):
    """
    Use a generator expression and List comprehensions to create an interator.
    Iterator contains the result of comparing each row length with matrix
    length.
    all(iterable) function returna True when all elements in the given iterable
    are true. Otherwise returns False.
    More info on generators here: https://www.youtube.com/watch?v=bD05uGo_sVI

    Arguments:
        m {list} -- Matrix to be evaluated
    
    Raises:
        TypeError: If no matrix was provided as an argument.
    
    Returns:
        [Bool] -- If the matrix is square.
    """
    if not isinstance(m, list):
        raise TypeError
    return all(len(row) == len(m) for row in m)


def diag(m):
    """
    Get the diagonal elements of the matrix.

    Arguments:
        m {list} -- Matrix to look for it's diagonal
    
    Raises:
        TypeError: If no matrix was provided or matrix is not square.
    
    Returns:
        list -- Elements of the diagonal
    """
    if not is_square(m):
        raise TypeError
    return [m[i][i] for i in range(len(m))]


def negative(m):
    """
    Get the negative of a matrix: negating each element.
    
    Arguments:
        m {list} -- Metrix to negate.
    
    Returns:
        list -- The negation of m matrix.
    """
    return [ [ -m[row][column] for column in range(len(m[0])) ] for row in range(len(m)) ]


def subdiag(m):
    """
    Get the subdiagonal elements of the matrix.
    
    Arguments:
        m {list} -- Matrix to look for it's subdiagonal
    
    Raises:
        TypeError: If no matrix was provided or matrix is not square.
        ValueError: If matrix has not at least two rows.
    
    Returns:
        list -- Elements of the subdiagonal
    """
    if not is_square(m):
        raise TypeError
    if len(m) == 1:
        raise ValueError
    return [m[i][i-1] for i in range(1, len(m))]


def superdiag(m):
    """
    Get the superdiagonal elements of the matrix.
    
    Arguments:
        m {list} -- Matrix to look for it's superdiagonal
    
    Raises:
        TypeError: If no matrix was provided or matrix is not square.
        ValueError: If matrix has not at least two rows.
    
    Returns:
        list -- Elements of the superdiagonal
    """
    if not is_square(m):
        raise TypeError
    if len(m) == 1:
        raise ValueError
    return [m[i][i+1] for i in range(0, len(m)-1)]


def tr(m):
    """
    Get the trace of a Matrix: sum of the items in the matrix's diagonal.
    
    Arguments:
        m {liost} -- Matrix to calculate it's trace

    Raises:
        TypeError: If no matrix was provided or matrix is not square.

    Returns:
        int -- Sum of the elements in diagonal
    """
    if not is_square(m):
        raise TypeError
    return sum([m[i][i] for i in range(len(m))])


def transpose(m):
    """
    Get the transposed version of a Matrix: switch lines with columns.

    Simple version of the algorithm:
    result = []
    for column in range(len(m[0])):
        tmp = []
        for row in range(len(m)):
            tmp.append(m[row][column])
        result.append(tmp)
    return result

    Oneline version of the algorithm:
    - Use comprehension list to create a list for each column.
    - Use another comprehension list to create a list for each row.
    The trick is to construct rows starting iteration with columns.

    return [
        [
            m[row][column]
            for row in range(len(m))
        ]
        for column in range(len(m[0]))
    ]

    Arguments:
        m {list} -- Matrix to be evaluated
    
    Returns:
        list -- Matrix transposed
    """
    return [ [ m[row][column] for row in range(len(m)) ] for column in range(len(m[0])) ]
