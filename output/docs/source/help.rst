Help:
=====

The most efficient way to discover what does thi spackage do is to launch all
unit tests and take a look at tests names.

.. code-block:: python

    pytest -m unit -v \
        --junitxml=output/xunit-reports/xunit-result.xml \
        --cov-report html:output/coverage/html \
        --cov-report xml:output/coverage/coverage.xml \
        --cov-report annotate:output/coverage/annotate \
        --cov=funwithmaths/ tests/

    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_non_matrix_object_is_not_square PASSED                                                                                                                                             [  2%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x1_is_square PASSED                                                                                                                                                        [  5%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x2_is_not_square PASSED                                                                                                                                                    [  7%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_2x1_is_not_square PASSED                                                                                                                                                    [ 10%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_2x2_is_square PASSED                                                                                                                                                        [ 12%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_diag_of_no_matrix_object PASSED                                                                                                                                                    [ 15%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_diag_of_no_square_matrix PASSED                                                                                                                                                    [ 17%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_diag_of_matrix_1x1 PASSED                                                                                                                                                          [ 20%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_diag_of_matrix_2x2 PASSED                                                                                                                                                          [ 22%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_tr_of_no_matrix_object PASSED                                                                                                                                                      [ 25%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_tr_of_no_square_matrix PASSED                                                                                                                                                      [ 27%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_tr_of_matrix_1x1 PASSED                                                                                                                                                            [ 30%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_tr_of_matrix_2x2 PASSED                                                                                                                                                            [ 32%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_subdiag_of_no_matrix_object PASSED                                                                                                                                                 [ 35%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_subdiag_of_no_square_matrix PASSED                                                                                                                                                 [ 37%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_subdiag_of_matrix_1x1 PASSED                                                                                                                                                       [ 40%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_subdiag_of_matrix_2x2 PASSED                                                                                                                                                       [ 42%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_subdiag_of_matrix_3x3 PASSED                                                                                                                                                       [ 45%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_superdiag_of_no_matrix_object PASSED                                                                                                                                               [ 47%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_superdiag_of_no_square_matrix PASSED                                                                                                                                               [ 50%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_superdiag_of_matrix_1x1 PASSED                                                                                                                                                     [ 52%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_superdiag_of_matrix_2x2 PASSED                                                                                                                                                     [ 55%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_superdiag_of_matrix_3x3 PASSED                                                                                                                                                     [ 57%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_transpose_of_no_matrix_object PASSED                                                                                                                                               [ 60%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_transposed_of_matrix_1x2 PASSED                                                                                                                                                    [ 62%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_transposed_of_matrix_2x1 PASSED                                                                                                                                                    [ 65%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_transposed_of_matrix_3x3 PASSED                                                                                                                                                    [ 67%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_non_matrix_object_is_not_symmetrical PASSED                                                                                                                                        [ 70%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x2_is_not_symmetrical PASSED                                                                                                                                               [ 72%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x1_is_symmetrical PASSED                                                                                                                                                   [ 75%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_4x4_is_symmetrical PASSED                                                                                                                                                   [ 77%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_4x4_is_not_symmetrical PASSED                                                                                                                                               [ 80%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_non_matrix_object_is_not_antisymmetrical PASSED                                                                                                                                    [ 82%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x2_is_not_antisymmetrical PASSED                                                                                                                                           [ 85%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_4x4_is_antisymmetrical PASSED                                                                                                                                               [ 87%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_4x4_is_not_antisymmetrical PASSED                                                                                                                                           [ 90%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_non_matrix_object_can_not_be_negated PASSED                                                                                                                                        [ 92%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x1_negative PASSED                                                                                                                                                         [ 95%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_1x2_negative PASSED                                                                                                                                                         [ 97%]
    >>> tests/unit/algebra/test_matrix.py::TestMatrix::test_matrix_4x4_negated PASSED
