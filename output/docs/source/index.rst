Fun With Maths
==============

Abouth this
-----------

Fun with maths is just an experiment about me learning algebra and Python. No
intention to create a production ready library. Just for fun.

Guide:
------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   license
   help



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
