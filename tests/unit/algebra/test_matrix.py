#!/usr/bin/env python
# -*- coding: utf-8


from funwithmaths.algebra import matrix

import pytest


@pytest.mark.unit
class TestMatrix(object):

    MATRIX_1x1 = [[11]]

    MATRIX_1x1_ANTISYMMETRICAL = [[0]]

    MATRIX_1x1_NEGATIVE = [[-11]]

    MATRIX_1x2 = [[11, 12]]

    MATRIX_1x2_NEGATIVE = [[-11, -12]]

    MATRIX_1x2_TRANS = [[11],
                        [12]]

    MATRIX_2x1 = [[11],
                  [21] ]

    MATRIX_2x1_TRANS = [[11, 21]]

    MATRIX_2x2 = [[11, 12],
                  [21, 22] ]

    MATRIX_2x2_TRANS = [[11, 21],
                        [12, 22] ]

    MATRIX_3x3 = [[11, 12, 13],
                  [21, 22, 23],
                  [31, 32, 33] ]

    MATRIX_3x3_TRANS = [[11, 21, 31],
                        [12, 22, 32],
                        [13, 23, 33] ]

    MATRIX_3x3_SYMMETRICAL = [[11, 21, 31],
                              [21, 22, 23],
                              [31, 23, 33] ]

    MATRIX_4x4 = [[11, 12, 13, 14],
                  [21, 22, 23, 24],
                  [31, 32, 33, 34],
                  [41, 42, 43, 44] ]

    MATRIX_4x4_NEGATIVE= [[-11, -12, -13, -14],
                          [-21, -22, -23, -24],
                          [-31, -32, -33, -34],
                          [-41, -42, -43, -44] ]

    MATRIX_4x4_ANTISYMMETRICAL = [ [   0,  12,  13,  14],
                                   [ -12,   0,  23,  24],
                                   [ -13, -23,   0,  34],
                                   [ -14, -24, -34,   0] ]

    MATRIX_4x4_TRANS = [[11, 21, 31, 41],
                        [12, 22, 32, 42],
                        [13, 23, 33, 43],
                        [14, 24, 34, 44] ]

    MATRIX_4x4_SYMMETRICAL = [[11, 21, 31, 41],
                              [21, 22, 23, 24],
                              [31, 23, 33, 34],
                              [41, 24, 34, 44] ]


    def test_non_matrix_object_is_not_square(self):
        with pytest.raises(TypeError):
            matrix.is_square(None)
        assert "Non matrix object is not square."


    def test_matrix_1x1_is_square(self):
        assert matrix.is_square(self.MATRIX_1x1), "Matrix 1x1 is square."


    def test_matrix_1x2_is_not_square(self):
        assert matrix.is_square(self.MATRIX_1x2) is False, "Matrix 1x2 is not square."


    def test_matrix_2x1_is_not_square(self):
        assert matrix.is_square(self.MATRIX_2x1) is False, "Matrix 2x1 is not square."


    def test_matrix_2x2_is_square(self):
        assert matrix.is_square(self.MATRIX_2x2), "Matrix 2x2 is square."


    def test_diag_of_no_matrix_object(self):
        with pytest.raises(TypeError):
            matrix.diag(None)
        assert "Diagonal of no matrix object can not be calculated."


    def test_diag_of_no_square_matrix(self):
        with pytest.raises(TypeError):
            matrix.diag(self.MATRIX_2x1)
        assert "Diagonal of no sqaure matrix can not be calculated."


    def test_diag_of_matrix_1x1(self):
        assert matrix.diag(self.MATRIX_1x1) == [11], "Diagonal of matrix 1x1."


    def test_diag_of_matrix_2x2(self):
        assert matrix.diag(self.MATRIX_2x2) == [11, 22], "Diagonal of matrix 2x2."


    def test_tr_of_no_matrix_object(self):
        with pytest.raises(TypeError):
            matrix.tr(None)
        assert "Trace of no matrix object can not be calculated."


    def test_tr_of_no_square_matrix(self):
        with pytest.raises(TypeError):
            matrix.tr(self.MATRIX_2x1)
        assert "Trace of no sqaure matrix can not be calculated."


    def test_tr_of_matrix_1x1(self):
        assert matrix.tr(self.MATRIX_1x1) == 11, "Trace of matrix 1x1."


    def test_tr_of_matrix_2x2(self):
        assert matrix.tr(self.MATRIX_2x2) == 33, "Trace of matrix 2x2."


    def test_subdiag_of_no_matrix_object(self):
        with pytest.raises(TypeError):
            matrix.subdiag(None)
        assert "Subiagonal of no matrix object can not be calculated."


    def test_subdiag_of_no_square_matrix(self):
        with pytest.raises(TypeError):
            matrix.subdiag(self.MATRIX_2x1)
        assert "Subdiagonal of no sqaure matrix can not be calculated."


    def test_subdiag_of_matrix_1x1(self):
        with pytest.raises(ValueError):
            matrix.subdiag(self.MATRIX_1x1)
        assert "Subdiagonal of matrix 1x1 can not be calculated."


    def test_subdiag_of_matrix_2x2(self):
        assert matrix.subdiag(self.MATRIX_2x2) == [21], "Subdiagnonal of matrix 2x2."


    def test_subdiag_of_matrix_3x3(self):
        assert matrix.subdiag(self.MATRIX_3x3) == [21, 32], "Subdiagnonal of matrix 3x3."


    def test_superdiag_of_no_matrix_object(self):
        with pytest.raises(TypeError):
            matrix.superdiag(None)
        assert "Superdiagonal of no matrix object can not be calculated."


    def test_superdiag_of_no_square_matrix(self):
        with pytest.raises(TypeError):
            matrix.superdiag(self.MATRIX_2x1)
        assert "Superdiagonal of no sqaure matrix can not be calculated."


    def test_superdiag_of_matrix_1x1(self):
        with pytest.raises(ValueError):
            matrix.superdiag(self.MATRIX_1x1)
        assert "Superdiagonal of matrix 1x1 can not be calculated."


    def test_superdiag_of_matrix_2x2(self):
        assert matrix.superdiag(self.MATRIX_2x2) == [12], "Superdiagnonal of matrix 2x2."


    def test_superdiag_of_matrix_3x3(self):
        assert matrix.superdiag(self.MATRIX_3x3) == [12, 23], "Superdiagnonal of matrix 3x3."


    def test_transpose_of_no_matrix_object(self):
        with pytest.raises(TypeError):
            matrix.transpose(None)
        assert "Transposed of no matrix object can not be calculated."

    def test_transposed_of_matrix_1x2(self):
        assert matrix.transpose(self.MATRIX_1x2) == self.MATRIX_1x2_TRANS, "Transposed of matrix 1x2."


    def test_transposed_of_matrix_2x1(self):
        assert matrix.transpose(self.MATRIX_2x1) == self.MATRIX_2x1_TRANS, "Transposed of matrix 2x1."


    def test_transposed_of_matrix_3x3(self):
        assert matrix.transpose(self.MATRIX_3x3) == self.MATRIX_3x3_TRANS, "Transposed of matrix 3x3."


    def test_non_matrix_object_is_not_symmetrical(self):
        with pytest.raises(TypeError):
            matrix.is_symmetrical(None)
        assert "Non matrix object is not symmetrical."


    def test_matrix_1x2_is_not_symmetrical(self):
        with pytest.raises(TypeError):
            matrix.is_symmetrical(self.MATRIX_1x2)
        assert "Matrix 1x2 is not symmetrical."


    def test_matrix_1x1_is_symmetrical(self):
        assert matrix.is_symmetrical(self.MATRIX_1x1), "Matrix 1x1 is symmetrical."


    def test_matrix_4x4_is_symmetrical(self):
        assert matrix.is_symmetrical(self.MATRIX_4x4_SYMMETRICAL), "Matrix 4x4 is symmetrical."


    def test_matrix_4x4_is_not_symmetrical(self):
        assert matrix.is_symmetrical(self.MATRIX_4x4) is False, "Matrix 4x4 is not symmetrical."


    def test_non_matrix_object_is_not_antisymmetrical(self):
        with pytest.raises(TypeError):
            matrix.is_antisymmetrical(None)
        assert "Non matrix object is not antisymmetrical."


    def test_matrix_1x2_is_not_antisymmetrical(self):
        with pytest.raises(TypeError):
            matrix.is_antisymmetrical(self.MATRIX_1x2)
        assert "Matrix 1x2 is not antisymmetrical."


    def test_matrix_4x4_is_antisymmetrical(self):
        assert matrix.is_antisymmetrical(self.MATRIX_4x4_ANTISYMMETRICAL), "Matrix 4x4 is antisymmetrical."


    def test_matrix_4x4_is_not_antisymmetrical(self):
        assert matrix.is_antisymmetrical(self.MATRIX_4x4) is False, "Matrix 4x4 is not antisymmetrical."


    def test_non_matrix_object_can_not_be_negated(self):
        with pytest.raises(TypeError):
            matrix.negative(None)
        assert "Non matrix object can not be negated."


    def test_matrix_1x1_negative(self):
        assert matrix.negative(self.MATRIX_1x1) == self.MATRIX_1x1_NEGATIVE, "Matrix 1x1 negated."


    def test_matrix_1x2_negative(self):
        assert matrix.negative(self.MATRIX_1x2) == self.MATRIX_1x2_NEGATIVE, "Matrix 1x2 negated."


    def test_matrix_4x4_negated(self):
        assert matrix.negative(self.MATRIX_4x4) == self.MATRIX_4x4_NEGATIVE, "Matrix 4x4 negated."
