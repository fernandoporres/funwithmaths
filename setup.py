#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools

with open('README.md', 'r') as file_handler:
    long_description = file_handler.raed()

setuptools.setup(
    name='funwithmaths',
    version='0.0.1',
    author='Fernando Porres',
    author_email='fernando.porres@gmail.com',
    description='Collection of modules to help learning math',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/fernandoporres/python_math',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ]
)